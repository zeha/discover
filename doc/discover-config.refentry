  <refmeta>
    <refentrytitle>discover-config</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>discover-config</refname>
    <refpurpose>config script</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <cmdsynopsis>
      <command>discover-config</command>
      <arg choice="opt">options</arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1>
    <title>Description</title>

    <para><command>discover-config</command> reports
    stuff about the package's configuration.</para>
  </refsect1>

  <refsect1>
    <title>Options</title>

    <variablelist>
      <varlistentry>
        <term><option>--VAR</option></term>
        <listitem>
          <para>Print the value of the variable VAR followed by a
          newline.  See below for a list of available
          variables.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-l, --list-vars</option></term>
        <listitem>
          <para>List the available variables.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1>
    <title>Variables</title>

    <itemizedlist>

      <listitem>
        <para><varname>major_version</varname></para>
      </listitem>

      <listitem>
        <para><varname>minor_version</varname></para>
      </listitem>

      <listitem>
        <para><varname>micro_version</varname></para>
      </listitem>

      <listitem>
        <para><varname>version</varname></para>
      </listitem>

      <listitem>
        <para><varname>cppflags</varname></para>
      </listitem>

      <listitem>
        <para><varname>ldflags</varname></para>
      </listitem>

      <listitem>
        <para><varname>libs</varname></para>
      </listitem>

      <listitem>
        <para><varname>prefix</varname></para>
      </listitem>

      <listitem>
        <para><varname>exec_prefix</varname></para>
      </listitem>

      <listitem>
        <para><varname>bindir</varname></para>
      </listitem>

      <listitem>
        <para><varname>sbindir</varname></para>
      </listitem>

      <listitem>
        <para><varname>libexecdir</varname></para>
      </listitem>

      <listitem>
        <para><varname>datadir</varname></para>
      </listitem>

      <listitem>
        <para><varname>sysconfdir</varname></para>
      </listitem>

      <listitem>
        <para><varname>sharedstatedir</varname></para>
      </listitem>

      <listitem>
        <para><varname>localstatedir</varname></para>
      </listitem>

      <listitem>
        <para><varname>libdir</varname></para>
      </listitem>

      <listitem>
        <para><varname>includedir</varname></para>
      </listitem>

      <listitem>
        <para><varname>oldincludedir</varname></para>
      </listitem>

      <listitem>
        <para><varname>infodir</varname></para>
      </listitem>

      <listitem>
        <para><varname>mandir</varname></para>
      </listitem>

      <listitem>
        <para><varname>build</varname></para>
      </listitem>

      <listitem>
        <para><varname>build_cpu</varname></para>
      </listitem>

      <listitem>
        <para><varname>build_vendor</varname></para>
      </listitem>

      <listitem>
        <para><varname>build_os</varname></para>
      </listitem>

      <listitem>
        <para><varname>host</varname></para>
      </listitem>

      <listitem>
        <para><varname>host_cpu</varname></para>
      </listitem>

      <listitem>
        <para><varname>host_vendor</varname></para>
      </listitem>

      <listitem>
        <para><varname>host_os</varname></para>
      </listitem>

      <listitem>
        <para><varname>target</varname></para>
      </listitem>

      <listitem>
        <para><varname>target_cpu</varname></para>
      </listitem>

      <listitem>
        <para><varname>target_vendor</varname></para>
      </listitem>

      <listitem>
        <para><varname>target_os</varname></para>
      </listitem>

      <listitem>
        <para><varname>lt_current</varname></para>
      </listitem>

      <listitem>
        <para><varname>lt_revision</varname></para>
      </listitem>

      <listitem>
        <para><varname>lt_age</varname></para>
      </listitem>

    </itemizedlist>
  </refsect1>
