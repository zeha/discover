/**
 * @file core.c
 * @brief Core and miscellaneous routines
 *
 * This file provides core routines that don't really fit anyplace
 * else.  There are likely some other routines that really should be in
 * here.
 *
 */

/* $Progeny$
 *
 * Copyright 2002 Progeny Linux Systems, Inc.
 * Copyright 2002 Hewlett-Packard Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <assert.h>

#include <discover/discover.h>
#include <discover/discover-xml.h>

#include <discover/utils.h>

static char *discover_errlist[] = {
    N_("Success."),
    N_("Input/output error."),
    N_("XML parsing error."),
    N_("System error."),
    N_("Disabled bus."),
    N_("Bus not found."),
    N_("Data source not found."),
    N_("Device not found."),
    N_("Invalid version range."),
    N_("Not implemented.")
};
static int
discover_nerr = sizeof(discover_errlist) / sizeof(discover_errlist[0]);

/**
 * @defgroup core Core functionality
 * @{
 */

/**
 * Allocate and initialize a discover_error_t structure.
 */
discover_error_t *
discover_error_new(void)
{
    discover_error_t *status = _discover_xmalloc(sizeof(discover_error_t));
    status->code = 0;
    status->message = NULL;
    status->create_message = _discover_create_message;
    return status;
}

/**
 * Allocate memory for a char * variable.
 * This routine was written for discover_error_t struct.
 */
void *
_discover_create_message(discover_error_t **status, char *message)
{
    if((*status)->message) {
        free((*status)->message);
    }
    (*status)->message = _discover_xmalloc(strlen(message) + 1);
    strcpy((*status)->message, message);
}

/**
 * Free a discover_error_t structure.
 */
void
discover_error_free(discover_error_t *status)
{
    assert(status != NULL);
    if (status->message != NULL) {
        free(status->message);
    }
    free(status);
}

/**
 * Convert an error code to a human-readable string localized
 * according to the current locale (XXX: no i18n yet).
 *
 * @param err Error code to convert
 */
char *
discover_strerror(discover_error_t *err)
{
    assert(err != NULL);
    assert(err->code >= 0);
    assert(err->code < discover_nerr);

    return _(discover_errlist[err->code]);
}

/**
 * Get the major version number.
 */
int
discover_major_version(void)
{
    return PACKAGE_MAJOR;
}

/**
 * Get the minor version number.
 */
int
discover_minor_version(void)
{
    return PACKAGE_MINOR;
}

/**
 * Get the micro version number.
 */
int
discover_micro_version(void)
{
    return PACKAGE_MICRO;
}

/** @} */
