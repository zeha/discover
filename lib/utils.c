/**
 * @file utils.c
 * @brief Helper utilities
 */

/* $Progeny$
 *
 * Copyright 2002 Hewlett-Packard Company
 * Copyright 2002 Progeny Linux Systems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include "config.h"

#include <sys/types.h>

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include <discover/discover.h>
#include <discover/utils.h>

void *
_discover_xmalloc(size_t size)
{
    void *result;

    /*
     * SUSv3 leaves the behavior when size == 0 undefined, so we'll protect
     * against that scenario here.
     */
    assert(size > 0);

    result = malloc(size);
    if (!result) {
        perror("malloc");
        exit(EX_OSERR);
    }

    return result;
}

void *
_discover_xrealloc(void *ptr, size_t size)
{
    void *result;

    assert(size >= 0);

    result = realloc(ptr, size);
    if (!result) {
        perror("realloc");
        exit(EX_OSERR);
    }

    return result;
}

char *
_discover_xstrdup(const char *s)
{
    size_t len;
    char *copy;

    assert(s != NULL);

    len = strlen(s) + 1;
    copy = _discover_xmalloc(len);
    memcpy(copy, s, len);

    return copy;
}

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
