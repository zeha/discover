# $Progeny$

.POSIX:

# Build paths
VPATH=			@srcdir@
builddir=		@builddir@
abs_builddir=		@abs_builddir@
top_builddir=		@top_builddir@
abs_top_builddir=	@abs_top_builddir@
srcdir=			@srcdir@
abs_srcdir=		@abs_srcdir@
top_srcdir=		@top_srcdir@
abs_top_srcdir=		@abs_top_srcdir@

# Install paths
prefix=			@prefix@
exec_prefix=		@exec_prefix@
bindir=			@bindir@
sbindir=		@sbindir@
libexecdir=		@libexecdir@
datadir=		@datadir@
sysconfdir=		@sysconfdir@
sharedstatedir=		@sharedstatedir@
localstatedir=		@localstatedir@
libdir=			@libdir@
includedir=		@includedir@
infodir=		@infodir@
mandir=			@mandir@

# Distribution
PACKAGE_TARNAME=	@PACKAGE_TARNAME@
PACKAGE_VERSION=	@PACKAGE_VERSION@

distname=		${PACKAGE_TARNAME}-${PACKAGE_VERSION}
distdir=		${top_builddir}/${distname}

# Program flags
CFLAGS=			@CFLAGS@
CPPFLAGS=		@DEFS@ @CPPFLAGS@
LDFLAGS=		@LDFLAGS@
LIBS=			@LIBS@

# Programs
CC=			@CC@
CPP=			@CPP@
INSTALL=		@INSTALL@
INSTALL_DIR=		@INSTALL_DIR@
PYTHON=		@PYTHON@
UNINSTALL_FILE=	@UNINSTALL_FILE@
UNINSTALL_DIR=	@UNINSTALL_DIR@


# Libtool programs
LIBTOOL=		sh ${top_builddir}/libtool
LTCLEAN=		${LIBTOOL} --mode=clean rm -rf
LTCOMPILE=		${LIBTOOL} --mode=compile ${CC} -c ${CPPFLAGS} ${CFLAGS}
LTLINK=			${LIBTOOL} --mode=link ${CC} ${LDFLAGS}
INSTALL_PROGRAM=	${LIBTOOL} --mode=install ${INSTALL} -m 555
INSTALL_LIB=		${LIBTOOL} --mode=install ${INSTALL} -m 555
UNINSTALL_LIB=		${LIBTOOL} --mode=uninstall rm -f

# install(1) programs
INSTALL_DATA=		${INSTALL} -m 444
INSTALL_SCRIPT=		${INSTALL} -m 555

###############################################################################
# Extra libraries

# Path to the portability library
libportability=		@libportability@

# Check
CHECK_CFLAGS=		@CHECK_CFLAGS@
CHECK_LIBS=		@CHECK_LIBS@

###############################################################################
# Rules

.SUFFIXES: .c .lo
.c.lo:
	${LTCOMPILE} -c $< && touch $@

.SUFFIXES: .py .pyc
.py.pyc:
	${PYTHON} -c "import py_compile; py_compile.compile('$<')" && touch $@
