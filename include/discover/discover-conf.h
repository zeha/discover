/**
 * @file discover-conf.h
 * @brief Public interface for reading Discover configuration data
 *
 * These routines are the public interface used to read the
 * configuration data from the desired source.  These interfaces are also
 * used to alter the URLs searched for configuration data.
 */

/* $Progeny$
 *
 * Copyright 2002 Hewlett-Packard Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef DISCOVER_CONF_H
#define DISCOVER_CONF_H

#include <discover/discover.h>

#ifdef __cplusplus
    extern "C" {
#endif

void discover_conf_load(discover_error_t *status);
discover_bus_map_t *discover_conf_get_full_bus_map(discover_error_t *status);
discover_bus_map_t *discover_conf_get_bus_map(discover_bus_t bus,
                                              discover_error_t *status);
discover_bus_map_t *discover_conf_get_bus_map_by_name(char *name,
                                                      discover_error_t *status);

void discover_conf_insert_url(char *url, discover_error_t *status);
void discover_conf_append_url(char *url, discover_error_t *status);
discover_xml_url_t *discover_conf_get_urls(discover_error_t *status);
void discover_conf_free(void);

char *discover_conf_get_bus_name(discover_bus_t bus);
char *discover_conf_get_filetype_name(discover_filetype_t filetype);
int discover_conf_name_to_bus(char *name, discover_error_t *status);

#ifdef __cplusplus
    }
#endif

#endif
