/* $Progeny$ */

/*
 * Copyright 2002 Progeny Linux Systems, Inc.
 * Copyright 2002 Hewlett-Packard Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>

#include <check.h>

#include <discover/discover.h>
#include <discover/discover-xml.h>

static char *vendor = "102b";
static char *model = "0010";
static char *version_number_high = "2.0";
static char *version_number_low = "1.0";
static char *version_number_text = "2.4.18.ac4-686";
static char *version_range_exc_exc = "(1.0, 2.0)";
static char *version_range_inc_inc = "[1.0, 2.0]";
static char *version_range_exc_inc = "(1.0, 2.0]";
static char *version_range_inc_exc = "[1.0, 2.0)";
static char *version_range_text_inf = "(2.4, inf)";
static char *version_range_text_lim = "(2.4, 2.4.18]";

static int cmp;
static discover_device_t *device, *xml_devices;
static discover_error_t *status;

START_TEST(xml_find_device)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    device = discover_xml_find_device(xml_devices, vendor, model, status);
    fail_unless(device != NULL, NULL);
}
END_TEST

START_TEST(xml_find_device_vendor_only)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    device = discover_xml_find_device(xml_devices, vendor, NULL, status);
    fail_unless(device != NULL, NULL);
}
END_TEST

START_TEST(xml_find_device_model_only)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    device = discover_xml_find_device(xml_devices, NULL, model, status);
    fail_unless(device != NULL, NULL);
}
END_TEST

START_TEST(xml_version_cmp_exc_exc_high)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    discover_xml_version_cmp(version_range_exc_exc, version_number_high,
                             status);
    fail_unless(status->code == 0, status->message);
    fail_unless(!cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_exc_exc_low)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_exc_exc,
                                   version_number_low, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(!cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_inc_inc_high)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_inc_inc,
                                   version_number_high, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_inc_inc_low)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_inc_inc,
                                   version_number_low, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_exc_inc_high)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_exc_inc,
                                   version_number_high, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_exc_inc_low)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_exc_inc,
                                   version_number_low, status);
    fail_unless(!cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_inc_exc_high)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_inc_exc,
                                   version_number_high, status);
    fail_unless(!cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_inc_exc_low)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_inc_exc,
                                   version_number_low, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_text_inf)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_text_inf,
                                   version_number_text, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(cmp, NULL);
}
END_TEST

START_TEST(xml_version_cmp_text_lim)
{
    xml_devices = discover_xml_get_devices(PCI, status);
    fail_unless(status->code == 0, status->message);

    cmp = discover_xml_version_cmp(version_range_text_lim,
                                   version_number_text, status);
    fail_unless(status->code == 0, status->message);
    fail_unless(!cmp, NULL);
}
END_TEST

Suite *
make_xml_suite(void)
{
    Suite *s;
    TCase *tc_core;

    status = discover_error_new();

    tc_core = tcase_create("core");
    tcase_add_test(tc_core, xml_find_device);
    tcase_add_test(tc_core, xml_find_device_vendor_only);
    tcase_add_test(tc_core, xml_find_device_model_only);
    tcase_add_test(tc_core, xml_version_cmp_exc_exc_high);
    tcase_add_test(tc_core, xml_version_cmp_exc_exc_low);
    tcase_add_test(tc_core, xml_version_cmp_inc_inc_high);
    tcase_add_test(tc_core, xml_version_cmp_inc_inc_low);
    tcase_add_test(tc_core, xml_version_cmp_exc_inc_high);
    tcase_add_test(tc_core, xml_version_cmp_exc_inc_low);
    tcase_add_test(tc_core, xml_version_cmp_inc_exc_high);
    tcase_add_test(tc_core, xml_version_cmp_inc_exc_low);
    tcase_add_test(tc_core, xml_version_cmp_text_inf);
    tcase_add_test(tc_core, xml_version_cmp_text_lim);

    s = suite_create("xml");
    suite_add_tcase(s, tc_core);

    return s;
}
