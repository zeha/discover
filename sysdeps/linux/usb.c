/* $Progeny$ */

/* usb.c -- Scan the USB bus
 *
 * AUTHORS: John R. Daily <jdaily@progeny.com>
 *
 * Copyright 2002 Hewlett-Packard Company
 * Copyright 2001, 2002 Progeny Linux Systems, Inc.
 * Copyright (C) 1998-2000 MandrakeSoft
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <discover/sysdep.h>
#include <discover/utils.h>

/*
 * This function is specific to each sysdep.
 */
static void
_discover_sysdep_init(discover_sysdep_data_t *node)
{
    node->busclass = _discover_xmalloc(5);
    node->vendor = _discover_xmalloc(5);
    node->model = _discover_xmalloc(5);
}

discover_sysdep_data_t *
_discover_get_usb_raw(void)
{
    FILE *f;
    char *line = NULL;
    size_t len = 0;
    discover_sysdep_data_t *head = NULL, *node, *last = NULL;
    unsigned int id;
    char class[3], subclass[3];

    if ((f = fopen(PATH_PROC_USB, "r"))) {
        while (getline(&line, &len, f) >= 0) {
            node = _discover_sysdep_data_new();
            _discover_sysdep_init(node);

            if (strstr(line, "ProdID=")) {
                sscanf(line, "P:  Vendor=%s ProdID=%s Rev=%*2x.%*02x\n",
                       node->vendor, node->model);

                while (getline(&line, &len, f) >= 0){
                    if(strstr(line, "I: ")){
                        sscanf(line, "I:  If#=%*2d Alt=%*2d #EPs=%*2d "
                               "Cls=%2s(%*5c) Sub=%2s Prot=%*02x "
                               "Driver=%*s\n", class, subclass);
                        snprintf(node->busclass, 5, "%s%s", class, subclass);
                        break;
                    }
                }
                if (head == NULL) {
                    head = node;
                    last = head;
                } else {
                    last->next = node;
                    last = node;
                }
            } else {
                free(node->busclass);
                free(node->vendor);
                free(node->model);
                free(node);
            }
        }
        fclose(f);
        free(line);
    }

    return head;
}

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
